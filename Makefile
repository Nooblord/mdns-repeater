# Copyright 2020 Alex D (https://gitlab.com/Nooblord/)
# This is free software, licensed under the GNU General Public License v3.

include $(TOPDIR)/rules.mk

PKG_NAME:=mdns-repeater
PKG_VERSION:=1.00
PKG_RELEASE:=1
PKG_MAINTAINER:=Alex D <nooblord@tuta.io>
PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)
SOURCE_DIR:=./files

include $(INCLUDE_DIR)/package.mk

define Package/$(PKG_NAME)
	SECTION:=net
	CATEGORY:=Network
	TITLE:=mdns-repeater
	DEPENDS:=+libubox +libuci
endef

define Package/$(PKG_NAME)/description
	mDNS reflector
endef

define Package/$(PKG_NAME)/conffiles
	/etc/config/network
endef

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	cp $(SOURCE_DIR)/* $(PKG_BUILD_DIR)
	$(Build/Patch)
endef

define Build/Compile
	$(TARGET_CC) $(TARGET_CFLAGS) -o $(PKG_BUILD_DIR)/mdns-repeater.o -c $(PKG_BUILD_DIR)/mdns-repeater.c
	$(TARGET_CC) $(TARGET_LDFLAGS) -o $(PKG_BUILD_DIR)/$1 $(PKG_BUILD_DIR)/mdns-repeater.o
endef

define Package/$(PKG_NAME)/install
	$(INSTALL_DIR) $(1)/usr/bin
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/mdns-repeater $(1)/usr/bin
endef

$(eval $(call BuildPackage,$(PKG_NAME)))
